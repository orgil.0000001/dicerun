﻿using UnityEngine.UI;
using Orgil;

public class BlinkText : Mb {
    public float duration = 1;
    Text text;
    bool isAlphaDown = true;
    float t;
    void Start() {
        t = duration;
        text = go.Txt();
    }
    void Update() {
        t += isAlphaDown ? -Dt : Dt;
        isAlphaDown = t < 0 || t > duration ? !isAlphaDown : isAlphaDown;
        text.color = text.color.A(t / duration);
    }
}