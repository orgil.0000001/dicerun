namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, PowerUp, Path, Win, Diamond, Coin, Bot }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Water = 4, UI = 5; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Water = 16, UI = 32; }
}