﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Bot : Character {
    private void Start() {
        BotRndTm();
        Init();
    }
    private void Update() {
        if (IsPlaying) {
            if (isTurn) {
                botT += Dt;
                if (botT > botTm) {
                    botT = 0;
                    StaJump();
                }
                Jump();
                DiceRot();
            }
        }
    }
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Win)) {
            Anim("Victory");
            Gc.I.GameOver();
        } else if (other.Tag(Tag.PowerUp)) {
            Ls.I.Emoji(true);
            PowerUp(other);
        }
    }
}