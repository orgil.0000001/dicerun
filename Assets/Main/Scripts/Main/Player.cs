﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using System.IO;
using System.Linq;

public class Player : Character {
    public static Player I {
        get {
            if (_.Null())
                _ = GameObject.FindObjectOfType<Player>();
            return _;
        }
    }
    static Player _;
    private void Awake() {
        _ = this;
    }
    public void Reset() {
    }
    void Start() {
        Turn(true);
    }
    void Update() {
        if (IsPlaying) {
            if (isTurn) {
                if (IsMbD)
                    StaJump();
                Jump();
                DiceRot();
            }
        }
    }
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Win)) {
            Anim("Victory");
            Gc.I.LevelCompleted();
        } else if (other.Tag(Tag.PowerUp)) {
            Ls.I.Emoji(false);
            PowerUp(other);
        }
    }
}