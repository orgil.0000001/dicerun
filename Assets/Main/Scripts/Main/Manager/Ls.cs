﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

[System.Serializable]
public class Level {
    public Color playerC, enemyC;
    public int playerDiceIdx, enemyDiceIdx;
    public Color pathC;
    public string playerPath, enemyPath;
    public int n;
    public Level(string playerPath, string enemyPath, int n) {
        Set(playerPath, enemyPath, n);
    }
    public void Upd() {
        Set(playerPath, enemyPath, n);
    }
    public void Set(string playerPath, string enemyPath, int n) {
        this.n = M.Max(n, playerPath.Length, enemyPath.Length);
        this.playerPath = playerPath + ' '.N(n - playerPath.Length);
        this.enemyPath = enemyPath + ' '.N(n - enemyPath.Length);
    }
};

[System.Serializable]
public class DiceC {
    public Color pwrUpC, pwrUpDefC, diceBgC, diceNumC;
    public DiceC(Color pwrUpC, Color pwrUpDefC, Color diceBgC, Color diceNumC) {
        this.pwrUpC = pwrUpC;
        this.pwrUpDefC = pwrUpDefC;
        this.diceBgC = diceBgC;
        this.diceNumC = diceNumC;
    }
}

public class DiceUI {
    public Image pwrUpAdd1Img, pwrUpAddPathImg, pwrUpMul2Img, diceBgImg, diceNumImg;
    public Text pwrUpAdd1Txt, pwrUpAddPathTxt, pwrUpMul2Txt;
    public DiceUI(Image pwrUpAdd1Img, Image pwrUpAddPathImg, Image pwrUpMul2Img, Image diceBgImg, Image diceNumImg, Text pwrUpAdd1Txt, Text pwrUpAddPathTxt, Text pwrUpMul2Txt) {
        this.pwrUpAdd1Img = pwrUpAdd1Img;
        this.pwrUpAddPathImg = pwrUpAddPathImg;
        this.pwrUpMul2Img = pwrUpMul2Img;
        this.diceBgImg = diceBgImg;
        this.diceNumImg = diceNumImg;
        this.pwrUpAdd1Txt = pwrUpAdd1Txt;
        this.pwrUpAddPathTxt = pwrUpAddPathTxt;
        this.pwrUpMul2Txt = pwrUpMul2Txt;
    }
}

public class Ls : Singleton<Ls> { // Level Spawner
    public Vector3 pathSz;
    public float pathSpc, playerSpc, pwrUpY, finishH, finishFlagH, jumpAnimSpd, diceRotTm;
    public GameObject pathPf, finishPf, pwrUpAdd1Pf, pwrUpAddPathPf, pwrUpMul2Pf, angryPf, smilePf;
    public GameObject playerUiGo, enemyUiGo;
    public Color pwrUpAdd1C, pwrUpAddPathC, pwrUpMul2C;
    public Bot bot;
    [HideInInspector]
    public DiceUI playerUi, enemyUi;
    [HideInInspector]
    public GameObject playerFinishGo, enemyFinishGo;
    public List<DiceC> diceCols;
    public List<Sprite> diceSprs;
    public List<Level> levels;
    public bool useLvl = false;
    public int lvl = 1;
    int Lvl => GetLvl(Gc.Level, levels.Count, 1, levels.Count);
    int LvlIdx => Lvl - 1;
    [HideInInspector]
    public List<GameObject> playerPaths = new List<GameObject>(), enemyPaths = new List<GameObject>();
    [HideInInspector]
    public Level l;
    public void Init() {
        if (useLvl)
            Gc.Level = lvl;
        LoadLevel();
        LeaderBoardData.SetDatas();
    }
    public void Emoji(bool isSmile) {
        Dst(Ins(isSmile ? smilePf : angryPf, enemyUiGo.transform), 1.34f);
    }
    public void LoadLevel() {
        playerUi = DiceUI(playerUiGo);
        enemyUi = DiceUI(enemyUiGo);
        l = levels[LvlIdx];
        l.Upd();
        UpdDiceC(true);
        UpdDiceUI(true, 0, 0, 0);
        Player.I.UpdCol(l.playerC);
        Player.I.Tp = PathPos(true, 0);
        Cm.I.UpdOff(Player.I.tf, Player.I.tf);
        UpdDiceC(false);
        UpdDiceUI(false, 0, 0, 0);
        bot.UpdCol(l.enemyC);
        bot.Tp = PathPos(false, 0);
        Es.I.ChildShow(0, (Gc.Level - 1) % 5);
        for (int i = 0; i < 2; i++) {
            bool isPlayer = i == 0;
            string path = isPlayer ? l.playerPath : l.enemyPath;
            for (int j = 0; j <= l.n + 1; j++) {
                Vector3 pos = PathPos(isPlayer, j), pwrUpPos = pos + V3.Y(pwrUpY);
                CrtPath(isPlayer);
                if (j == l.n + 1) {
                    GameObject finishGo = Ins(finishPf, pos, Q.O, tf);
                    finishGo.Bc(V3.V(pathSz.x, finishH, 0.2f), V3.Y(finishH / 2), true, null, GcTp.Add);
                    finishGo.Child(0).TlpX(pathSz.x / -2);
                    finishGo.Child(0).Tls(0.02f, finishH, 0.02f);
                    finishGo.Child(1).TlpX(pathSz.x / 2);
                    finishGo.Child(1).Tls(0.02f, finishH, 0.02f);
                    finishGo.Child(2).TlpY(finishH);
                    finishGo.Child(2).Tls(pathSz.x, finishFlagH, 0.01f);
                    if (isPlayer)
                        playerFinishGo = finishGo;
                    else
                        enemyFinishGo = finishGo;
                } else if (j > 0) {
                    if (path[j - 1] == '1')
                        CrtPwrUp(pwrUpAdd1Pf, pwrUpPos, pwrUpAdd1C);
                    else if (path[j - 1] == '2')
                        CrtPwrUp(pwrUpAddPathPf, pwrUpPos, pwrUpAddPathC);
                    else if (path[j - 1] == '3')
                        CrtPwrUp(pwrUpMul2Pf, pwrUpPos, pwrUpMul2C);
                }
            }
        }
    }
    public List<GameObject> Paths(bool isPlayer) {
        return isPlayer ? playerPaths : enemyPaths;
    }
    public GameObject Finish(bool isPlayer) {
        return isPlayer ? playerFinishGo : enemyFinishGo;
    }
    public void CrtPath(bool isPlayer) {
        List<GameObject> paths = Paths(isPlayer);
        GameObject pathGo = Ins(pathPf, PathPos(isPlayer, paths.Count), Q.O, tf);
        pathGo.Tls(pathSz);
        pathGo.Child(0).RenMatCol(l.pathC);
        paths.Add(pathGo);
    }
    public Vector3 PathPos(bool isPlayer, int i) {
        return V3.Xz((pathSz.x + playerSpc) * (isPlayer ? 0.5f : -0.5f), (pathSz.z + pathSpc) * i);
    }
    void CrtPwrUp(GameObject pf, Vector3 pos, Color c) {
        Transform pwrUptf = Ins(pf, pos, Q.O, tf).Child(0);
        for (int i = 0; i < pwrUptf.childCount; i++)
            pwrUptf.Child(i).RenMatCol(c);
    }
    public void UpdDiceUI(bool isPlayer, int pwrUpAdd1, int pwrUpAddPath, int pwrUpMul2) {
        UpdDiceUI(isPlayer ? playerUi : enemyUi, isPlayer ? l.playerDiceIdx : l.enemyDiceIdx, pwrUpAdd1, pwrUpAddPath, pwrUpMul2);
    }
    public void UpdDiceUI(DiceUI ui, int diceIdx, int pwrUpAdd1, int pwrUpAddPath, int pwrUpMul2) {
        DiceC c = diceCols[diceIdx];
        UpdPwrUp(ui.pwrUpAdd1Img, ui.pwrUpAdd1Txt, pwrUpAdd1, c.pwrUpC, c.pwrUpDefC);
        UpdPwrUp(ui.pwrUpAddPathImg, ui.pwrUpAddPathTxt, pwrUpAddPath, c.pwrUpC, c.pwrUpDefC);
        UpdPwrUp(ui.pwrUpMul2Img, ui.pwrUpMul2Txt, pwrUpMul2, c.pwrUpC, c.pwrUpDefC);
    }
    public void UpdDiceNum(bool isPlayer, int dice) {
        (isPlayer ? playerUi : enemyUi).diceNumImg.sprite = diceSprs[dice - 1];
    }
    public void UpdDiceNum(DiceUI ui, int dice) {
        ui.diceNumImg.sprite = diceSprs[dice - 1];
    }
    DiceUI DiceUI(GameObject go) {
        return new DiceUI(go.Child(0).Img(), go.Child(1).Img(), go.Child(2).Img(), go.Child(3).Img(), go.Child(3, 0).Img(), go.Child(0, 0).Txt(), go.Child(1, 0).Txt(), go.Child(2, 0).Txt());
    }
    void UpdDiceC(bool isPLayer) {
        UpdDiceC(isPLayer ? playerUi : enemyUi, isPLayer ? l.playerDiceIdx : l.enemyDiceIdx);
    }
    void UpdDiceC(DiceUI ui, int diceIdx) {
        DiceC c = diceCols[diceIdx];
        ui.diceBgImg.color = c.diceBgC;
        ui.diceNumImg.color = c.diceNumC;
    }
    void UpdPwrUp(Image img, Text txt, int n, Color c, Color defC) {
        img.color = n > 0 ? c : defC;
        txt.text = n > 0 ? "" + n : "";
    }
    int GetLvl(int lvl, int lvlCnt, int rnd1, int rnd2) {
        if (Data.LevelData.S().IsNe())
            Data.LevelData.SetList(A.ListAp(1, 1, lvlCnt));
        List<int> lis = Data.LevelData.ListI();
        if (lvl <= lis.Count) {
            return lis[lvl - 1];
        } else {
            int prvLvl = lis.Last();
            for (int i = lis.Count + 1; i <= lvl; i++) {
                prvLvl = prvLvl == rnd1 ? Rnd.RngIn(rnd1 + 1, rnd2) : prvLvl == rnd2 ? Rnd.RngIn(rnd1, rnd2 - 1) : Rnd.P((prvLvl - rnd1).F() / (rnd2 - rnd1)) ? Rnd.RngIn(rnd1, prvLvl - 1) : Rnd.RngIn(prvLvl + 1, rnd2);
                lis.Add(prvLvl);
            }
            Data.LevelData.SetList(lis);
            return prvLvl;
        }
    }
}
// ░ ▒ ▓ █ ▄ ▀ ■
// ┌ ┬ ┐ ─ ╔ ╦ ╗ ═
// ├ ┼ ┤ │ ╠ ╬ ╣ ║
// └ ┴ ┘   ╚ ╩ ╝
// ⁰¹²³⁴⁵⁶⁷⁸⁹ ⁻⁺⁼⁽⁾ ⁱⁿ superscript
// ₀₁₂₃₄₅₆₇₈₉ ₋₊₌₍₎⨧ ᵣᵤₐᵢⱼₓₑᵥₒₔ ᵪᵧᵦᵨᵩ subscript
// _ ¯ ~ ≡ ‗ ¦ ¨ ¬ · |
// ñ Ñ @ ¿ ? ¡ ! : / \ frequently-used
// á é í ó ú Á É Í Ó Ú vowels acute accent
// ä ë ï ö ü Ä Ë Ï Ö Ü vowels with diaresis
// ½ ¼ ¾ ¹ ³ ² ƒ ± × ÷ mathematical symbols
// $ £ ¥ ¢ ¤ ® © ª º ° commercial / trade symbols
// " ' ( ) [ ] { } « » quotes and parenthesis