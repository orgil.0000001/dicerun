﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Cm : Singleton<Cm> { // Camera Controller
    public static bool IsZoom = false;
    public Transform followTf;
    public bool isSmt = false;
    public float smtSpd = 5;
    Vector3 off, dOff;
    Transform follow2Tf;
    void Start() {
        IsZoom = false;
        UpdOff(followTf, followTf);
    }
    public void UpdOff(Transform followTf, Transform follow2Tf) {
        this.followTf = followTf;
        this.follow2Tf = follow2Tf;
        off = Tp - (followTf.position + follow2Tf.position) / 2;
        dOff = V3.O;
    }
    void Update() {
        if (IsPlaying && followTf) {
            if (isSmt) {
                Tp = V3.Lerp(Tp, off + dOff + ((followTf.position + follow2Tf.position) / 2).Y(0), Dt * smtSpd);
            } else {
                Tp = off + dOff + (followTf.position + follow2Tf.position) / 2;
            }
        }
    }
    public void Shake(float dur, float mag) { StaCor(ShakeCor(dur, mag)); }
    IEnumerator ShakeCor(float dur, float mag) {
        for (float t = 0; t < dur; t += Dt) {
            Camera.main.Tlp(go.TfDir(V3.Xy(Rnd.F1, Rnd.F1) * mag));
            yield return null;
        }
        Camera.main.Tlp(V3.O);
    }
    public void OffZoom(Vector3 pos, float mag, float dur) { StaCor(OffZoomCor(pos, V3.Dis(Tp, pos) / mag * dur)); }
    IEnumerator OffZoomCor(Vector3 pos, float dur) {
        Vector3 dOffEnd = Tp - (off + followTf.position);
        for (float t = 0; t < dur; t += Dt) {
            dOff = V3.Lerp(dOff, dOffEnd, t / dur);
            yield return null;
        }
        Tp = pos;
    }
    public void Zoom(Vector3 pos, float mag, float dur) { StaCor(ZoomCor(pos, V3.Dis(Tp, pos) / mag * dur)); }
    IEnumerator ZoomCor(Vector3 pos, float dur) {
        if (!IsZoom) {
            IsZoom = true;
            Vector3 staPos = Tp;
            for (float t = 0; t < dur; t += Dt) {
                Tp = V3.Lerp(staPos, pos, t / dur);
                yield return null;
            }
            Tp = pos;
            IsZoom = false;
        }
    }
}