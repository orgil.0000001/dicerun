﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using TMPro;

public class Character : Mb {
    public LeaderBoardData data;
    HWP hwp = null;
    TextMeshPro followTmp;
    protected bool isStaJump = false, isJump = false, isTurn = false, isDiceRot = false, isMul2 = false;
    protected int jump = 0, pwrUpAdd1 = 0, pwrUpAddPath = 0, pwrUpMul2 = 0, dice = 1, add = 0;
    protected float rotT = 0, botT, botTm;
    public Character chr => data.isPlayer ? (Character)Ls.I.bot : (Character)Player.I;
    public void Init() {
        tf.Par(Bs.I.transform);
    }
    public void StaJump() {
        if (!isStaJump) {
            isStaJump = true;
            isDiceRot = false;
            jump = dice;
        }
    }
    public void Jump() {
        if (!isJump && isStaJump) {
            if (jump > 0) {
                StaCor(JumpCor());
                jump--;
            } else {
                isStaJump = false;
                if (add > 0) {
                    if (!data.isPlayer)
                        BotRndTm();
                    Turn(true);
                    add--;
                } else {
                    Turn(false);
                }
            }
        }
    }
    public void BotRndTm() {
        botT = 0;
        botTm = Rnd.Rng(1f, 2.5f);
    }
    public void Turn(bool isT) {
        isTurn = isT;
        isDiceRot = isT;
        chr.isTurn = !isT;
        chr.isDiceRot = !isT;
    }
    public void DiceRot() {
        if (isDiceRot) {
            rotT += Dt;
            if (rotT > Ls.I.diceRotTm) {
                dice = Rnd.Idx(dice - 1, 6) + 1;
                Ls.I.UpdDiceNum(data.isPlayer, dice);
                rotT = 0;
            }
        }
    }
    public void Anim(string name) {
        go.Child(0).An().Play(name);
    }
    public void UpdCol(Color c) {
        go.Child(0, 0).RenMats().ForEach(x => x.color = c);
    }
    public void PowerUp(Collider other) {
        PowerUpTp tp = other.Gc<PowerUp>().tp;
        if (tp == PowerUpTp.Add1) {
            add++;
            pwrUpAdd1++;
        } else if (tp == PowerUpTp.AddPath) {
            for (int i = 0; i < 2; i++)
                Ls.I.CrtPath(!data.isPlayer);
            Ls.I.Finish(!data.isPlayer).Tp(Ls.I.Paths(!data.isPlayer).Last().Tp());
            pwrUpAddPath++;
        } else {
            pwrUpMul2++;
            jump += dice;
        }
        Ls.I.UpdDiceUI(data.isPlayer, pwrUpAdd1, pwrUpAddPath, pwrUpMul2);
        Dst(other.gameObject);
    }
    IEnumerator JumpCor() {
        isJump = true;
        Anim("Jump");
        float s = 140 / 60f / Ls.I.jumpAnimSpd, tm = s * 0.3f;
        yield return Wf.S(s * 0.35f);
        Vector3 pos = Tp, pos2 = pos + V3.Z(Ls.I.pathSz.z + Ls.I.pathSpc);
        for (float t = 0; t < tm; t += Dt) {
            Tp = V3.Move(pos, pos2, t / tm);
            yield return null;
        }
        Tp = pos2;
        yield return Wf.S(s * 0.35f);
        isJump = false;
    }
    public void UpdatePlace(int place) { }
    public void UpdateColor(Color col) { }
    // default functions
    public void CreateName(LeaderBoardData lbd) {
        UpdateColor(lbd.col);
        data = lbd;
        Color c = data.isPlayer ? Ls.I.l.playerC : Ls.I.l.enemyC;
        if (Gc.I.followType != FollowType.None) {
            hwp = go.Gc<HWP>();
            if (hwp) {
                hwp.info.arrow.size = Screen.width * 0.06f;
                HWPManager.I.TextStyle.fontSize = M.RoundI(Screen.width * 0.06f);
                //HWPManager.I.TextStyle.contentOffset = V2.u * -(Screen.width * 0.06f * 1.6f);
                hwp.info.text = Gc.I.isNameUppercase ? data.name.ToUpper() : data.name;
                hwp.info.color = c;
                hwp.info.character = this;
            }
            if (Gc.I.followType == FollowType.Follow || Gc.I.followType == FollowType.FollowPointer) {
                Follow follow = Crt.Pf<Follow>(A.LoadGo("Main/FollowName"), new Tf(Tp), Gc.I.transform);
                follow.followTf = transform;
                followTmp = follow.Child<TextMeshPro>(0);
                if (followTmp) {
                    followTmp.text = Gc.I.isNameUppercase ? data.name.ToUpper() : data.name;
                    followTmp.color = c;
                }
            }
        }
    }
    public void RemoveName() {
        if (Gc.I.followType != FollowType.None) {
            hwp.info.text = "";
            hwp.info.arrow.size = 0;
            if ((Gc.I.followType == FollowType.Follow || Gc.I.followType == FollowType.FollowPointer) && followTmp)
                followTmp.text = "";
        }
    }
}